package com.example.myshoppinglist;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface noteDao {
    @Insert
    void insert(ShoppingList user);
    @Delete
    void delete(ShoppingList user);
    @Insert
    void insert(ShoppingListDetail user);
    @Delete
    void delete(ShoppingListDetail user);
    @Query("select * from ShoppingList")
    List<ShoppingList> getAllLists();

    @Query("select * from ShoppingListDetail where listId=:listId ")
    List<ShoppingListDetail> getListDetail(int listId);
}
