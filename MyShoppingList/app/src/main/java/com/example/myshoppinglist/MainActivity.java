package com.example.myshoppinglist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private EditText name,password;
    private String nameStr, passwordStr;
    private DatabaseReference reference;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Getting the firebase refrences
        reference= FirebaseDatabase.getInstance().getReference("credidentials");
        auth=FirebaseAuth.getInstance();
        name= findViewById(R.id.nameFielsLogin);
        password= findViewById(R.id.passwordFeildLogin);
        FirebaseUser user=auth.getCurrentUser();
        //If condition for user is already login or not
        if(user!=null)
        {
            //If user has already login take user to MapActivity
            Intent i=new Intent(MainActivity.this,MapActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void loginFun(View view) {


        nameStr = name.getText().toString().toLowerCase();
        if(nameStr.isEmpty()){
            Toast.makeText(this, "Enter User Name", Toast.LENGTH_SHORT).show();
        }
        else{
            //Login user by using firebase refrence
            passwordStr = password.getText().toString();
            reference.child(nameStr)
                    .addListenerForSingleValueEvent(listener);
        }



    }


    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()){
                String pass=dataSnapshot.child("passwordStr").getValue(String.class);
                String email=dataSnapshot.child("emailStr").getValue(String.class);
                if(pass.equals(passwordStr)){
                    auth.signInWithEmailAndPassword(email,pass)
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    Intent i=new Intent(MainActivity.this,MapActivity.class);
                                    startActivity(i);
                                }
                            });

                }
                else{
                    Toast.makeText(MainActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(MainActivity.this, "Record Not Found", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Toast.makeText(MainActivity.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
        }
    };

    public void createAccFun(View view) {
        Intent in=new Intent(MainActivity.this, SignupActivity.class);
        startActivity(in);
    }
}

