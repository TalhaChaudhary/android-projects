package com.example.myshoppinglist;
import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class noteViewModel extends AndroidViewModel {
    private String TAG=this.getClass().getSimpleName();
    public noteDao notedao;
    public noteRoomDatabase database;
    public ArrayList<ShoppingListDetail> listDetails;
    public noteViewModel(@NonNull Application application) {
        super(application);
        database=noteRoomDatabase.getDatabase(application);
        notedao=database.notDao();
    }
    public List<ShoppingListDetail> getListDetail(int listId)
    {
        try {
            return new getListDetailAsyncTask(notedao).execute(listId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<ShoppingList> getAllLists()
    {
        try {
            return new getAllListAsyncTask(notedao).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void insert(ShoppingList list)
    {
        new InsertAsyncTask(notedao).execute(list);
    }
    public void insert(ArrayList<ShoppingListDetail> list)
    {
        listDetails=new ArrayList<>();
        listDetails=list;
        new InsertListDetailAsyncTask(notedao).execute();

    }
    @Override
    protected void onCleared()
    {
        super.onCleared();
    }


    private class getAllListAsyncTask extends  AsyncTask<Void,Void,List<ShoppingList>>
    {
        noteDao mnotedao;
        public getAllListAsyncTask(noteDao mnotedao)
        {
            this.mnotedao=mnotedao;
        }
        @Override
        protected List<ShoppingList> doInBackground(Void... voids) {
           return mnotedao.getAllLists();
        }
    }
    private class getListDetailAsyncTask extends  AsyncTask<Integer,Integer,List<ShoppingListDetail>>
    {
        noteDao mnotedao;
        public getListDetailAsyncTask(noteDao mnotedao)
        {
            this.mnotedao=mnotedao;
        }
        @Override
        protected List<ShoppingListDetail> doInBackground(Integer... user) {
            return mnotedao.getListDetail(user[0].intValue());
        }
    }
    private class InsertAsyncTask extends AsyncTask<ShoppingList,Void,Void> {
        noteDao mnotedao;
        public InsertAsyncTask(noteDao mnotedao) {
            this.mnotedao = mnotedao;
        }
        @Override
        protected Void doInBackground(ShoppingList... list) {

                mnotedao.insert(list[0]);
            return null;
        }
    }
    private class InsertListDetailAsyncTask extends AsyncTask<Void,Void,Void> {
        noteDao mnotedao;
        public InsertListDetailAsyncTask(noteDao mnotedao) {
            this.mnotedao = mnotedao;
        }
        @Override
        protected Void doInBackground(Void... list) {
            for (int i=0;i<listDetails.size();i++)
            {
                mnotedao.insert(listDetails.get(i));
            }
            return null;
        }
    }
}

