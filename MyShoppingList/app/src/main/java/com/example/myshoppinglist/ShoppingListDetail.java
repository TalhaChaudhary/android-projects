package com.example.myshoppinglist;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ShoppingListDetail",primaryKeys={"itemId","listId"})
public class ShoppingListDetail {

    @NonNull
    int itemId;
    @NonNull
    int listId;
    String name;
    public ShoppingListDetail(int itemId, int listId, String name) {
        this.itemId = itemId;
        this.listId = listId;
        this.name = name;
    }

    public ShoppingListDetail() {
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
