package com.example.myshoppinglist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {
    private Context mContext;
    private List<ShoppingList> shoppingLists;
    public ShoppingListAdapter(Context mContext, List<ShoppingList> itemList) {
        this.mContext = mContext;
        shoppingLists = itemList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.shopping_list_view,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       final ShoppingList list=shoppingLists.get(position);
       holder.ListName.setText(list.getName());
       holder.itemView.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v, MotionEvent event) {
               Intent i= new Intent(mContext,ShowListDetailActivity.class);
               i.putExtra("listId",list.getId());
               i.putExtra("listName",list.getName());
               v.getContext().startActivity(i);
               return false;
               //here get data from room database;
           }
       });
    }
    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView ListName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ListName= itemView.findViewById(R.id.ItemText);
        }
    }
}
