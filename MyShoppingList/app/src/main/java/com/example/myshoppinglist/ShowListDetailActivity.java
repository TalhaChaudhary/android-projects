package com.example.myshoppinglist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ShowListDetailActivity extends AppCompatActivity {

    List<ShoppingListDetail> itemList;
    RecyclerView recyclerView;
    noteViewModel nvm;
    Button back;
    TextView listName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list_detail);
        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);
        listName=findViewById(R.id.shopping);
        Bundle b=getIntent().getExtras();
        //getting extras from intent
        int listId=b.getInt("listId");
        String name=b.getString("listName");
        listName.setText(name);
        itemList=new ArrayList<>();
        //Getting list detail from roomDatabase by providing list Id which we get from extras
        nvm= ViewModelProviders.of(this).get(noteViewModel.class);
        itemList=nvm.getListDetail(listId);
        //Toast.makeText(getApplicationContext(),""+listId,Toast.LENGTH_LONG).show();
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        ListDetailAdapter userAdapter=new ListDetailAdapter(ShowListDetailActivity.this,itemList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userAdapter);
        //Back button take you to the item list
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShowListDetailActivity.this,SoppingList.class);
                startActivity(i);
            }
        });

    }
}