package com.example.myshoppinglist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ListDetailAdapter extends RecyclerView.Adapter<ListDetailAdapter.ViewHolder> {
    private Context mContext;
    private List<ShoppingListDetail> ItemLists;

    public ListDetailAdapter(Context mContext, List<ShoppingListDetail> itemLists) {
        this.mContext = mContext;
        ItemLists = itemLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflate the list detail layout
        View view= LayoutInflater.from(mContext).inflate(R.layout.list_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Binding each item to shopping list detail
        final ShoppingListDetail list=ItemLists.get(position);
        holder.ItemName.setText(list.getName());
    }

    @Override
    public int getItemCount() {
        return ItemLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
    TextView ItemName;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        ItemName= itemView.findViewById(R.id.ItemText);
    }
}

}
