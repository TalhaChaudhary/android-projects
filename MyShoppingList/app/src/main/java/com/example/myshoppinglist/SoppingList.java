package com.example.myshoppinglist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SoppingList extends AppCompatActivity {

    public noteViewModel nvm;
    private List<ShoppingList> itemList;
    RecyclerView recyclerView;
    LinearLayout linearLayout;
    EditText name;
    Button next,createList;
    TextView heading;
    Button searchNearByMarkets;
    static int id=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sopping_list);
        nvm= ViewModelProviders.of(this).get(noteViewModel.class);
        //nvm.insert(new ShoppingList(+id,"List"+id));
        itemList=new ArrayList<>();
        searchNearByMarkets=findViewById(R.id.searchSuperMarkets);
        //getting all the items from roomDatabase
        setItemList();
        recyclerView=findViewById(R.id.recyclerView);
        linearLayout=findViewById(R.id.linearLayoutId);
        heading=findViewById(R.id.heading);
        name=findViewById(R.id.listName);
        createList=findViewById(R.id.createListbtn);
        next=findViewById(R.id.nextBtn);
        linearLayout.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        ShoppingListAdapter userAdapter=new ShoppingListAdapter(SoppingList.this,itemList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userAdapter);
        //onClick creatList button it will hite the recyclerView and show the createList linearLyout
        createList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.setVisibility(View.VISIBLE);
                createList.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                heading.setVisibility(View.GONE);
                searchNearByMarkets.setVisibility(View.GONE);
            }
        });
        //And then on click on next button it will take user to Item Activity where user will select the items for shopping list
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand=new Random();
                Intent i=new Intent(getApplicationContext(),ItemActivigty.class);
                i.putExtra("listName",name.getText().toString());
                i.putExtra("id",+rand.nextInt(100000));
                startActivity(i);
            }
        });
        //button will take to you map Activity if you are logIn already otherwise youhave to login or signup first
        searchNearByMarkets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(SoppingList.this,MainActivity.class);
                startActivity(i);

            }
        });
    }
    private void setItemList() {
        itemList=nvm.getAllLists();
    }
}