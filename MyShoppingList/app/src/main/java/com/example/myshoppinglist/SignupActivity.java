package com.example.myshoppinglist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {

    private EditText name, email, password, conPassword;
    private String nameStr, passwordStr, emailStr, conPasswordStr;
    private DatabaseReference refrence;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
//getting firebase refrence and authentication instance
        refrence= FirebaseDatabase.getInstance().getReference("credidentials");
        auth= FirebaseAuth.getInstance();
//Binding XML file with java objects
        name= findViewById(R.id.nameFiels);
        email= findViewById(R.id.emailFiels);
        password= findViewById(R.id.passwordFeild);
        conPassword= findViewById(R.id.confirmpasswordFeild);

    }
//Signup Function signUp user to firebase
    public void sigupFun(View view) {

        nameStr= name.getText().toString().toLowerCase();
        passwordStr= password.getText().toString();
        emailStr= email.getText().toString();
        conPasswordStr= conPassword.getText().toString();

//Confirming Input Text field userName and Password
        if(passwordStr.equals(conPasswordStr)) {
            auth.createUserWithEmailAndPassword(emailStr,passwordStr)
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            User u = new User(auth.getCurrentUser().getUid() ,nameStr, passwordStr, emailStr);
                            refrence.child(nameStr).setValue(u)
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(SignupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(SignupActivity.this, "Data Sucessfully added", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            startActivity(new Intent(SignupActivity.this, MainActivity.class));
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(SignupActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

        }
        else{
            findViewById(R.id.confirmpasswordFeild).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
    }
}
