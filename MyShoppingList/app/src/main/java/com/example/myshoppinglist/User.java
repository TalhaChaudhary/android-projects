package com.example.myshoppinglist;

public class User {
    public String nameStr, passwordStr, emailStr;
    public  String uid;


    public User(String uid,String nameStr, String passwordStr, String emailStr) {
        this.nameStr = nameStr;
        this.uid=uid;
        this.passwordStr = passwordStr;
        this.emailStr = emailStr;
    }
}
