package com.example.myshoppinglist;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {ShoppingList.class,ShoppingListDetail.class},version = 1)
public abstract class noteRoomDatabase extends RoomDatabase {
    public abstract noteDao notDao();

    public static volatile noteRoomDatabase noteRoomInstance;

    public static noteRoomDatabase getDatabase(final Context context)
    {
        if(noteRoomInstance==null)
        {
            synchronized (noteRoomDatabase.class){
                if(noteRoomInstance==null){
                    noteRoomInstance= Room.databaseBuilder(context.getApplicationContext(),noteRoomDatabase.class,"note_database").build();
                }
            }
        }
        return noteRoomInstance;
    }
}
