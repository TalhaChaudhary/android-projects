package com.example.myshoppinglist;
//Item class using for simplicity by using oop concepts
public class Item {
    public int id;
    public String name;
    public int status;

    public Item() {
    }

    public Item(int id, String name, int status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
