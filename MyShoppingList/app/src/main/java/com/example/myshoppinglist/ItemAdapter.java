package com.example.myshoppinglist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

private Context mContext;
private List<Item> ItemList;

    public ItemAdapter(Context mContext, List<Item> itemList) {
        this.mContext = mContext;
        ItemList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_view,parent,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Item item = ItemList.get(position);
        holder.ItemName.setText(item.getName());
        holder.AddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(item.status==0) {
                    v.setBackgroundColor(mContext.getResources().getColor(R.color.green));
                    item.setStatus(1);
                }
                else{
                    v.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
                    item.setStatus(0);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return ItemList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView ItemName;
        Button AddBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ItemName= itemView.findViewById(R.id.ItemText);
            AddBtn=itemView.findViewById(R.id.Addbtn);
        }
    }

}
