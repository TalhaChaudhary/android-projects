package com.example.myshoppinglist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class ItemActivigty extends AppCompatActivity {

    public noteViewModel nvm;
    private List<Item> itemList;
    public RecyclerView recyclerView;
    Button btnCreate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_activigty);
        nvm= ViewModelProviders.of(this).get(noteViewModel.class);
        itemList=new ArrayList<>();
        //setting hard coded value of the items in itemList
        setItemList();
        recyclerView=findViewById(R.id.recyclerView);
        btnCreate=findViewById(R.id.createListbtn);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        ItemAdapter userAdapter=new ItemAdapter(ItemActivigty.this,itemList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userAdapter);
        //take the input from user for creating new shopping list by getting list status
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ShoppingListDetail> item=new ArrayList<>();
                Intent intent=new Intent(getApplicationContext(),SoppingList.class);
                //getting list name from previous activity
                Bundle b=getIntent().getExtras();
                String name=(String) b.getString("listName");
                int id=(int)b.getInt("id");
                for (int i=0;i<itemList.size();i++)
                {
                    if(itemList.get(i).getStatus()==1)
                    {
                        //item.add(itemList.get(i));
                        item.add(new ShoppingListDetail(itemList.get(i).getId(),id,itemList.get(i).name));
                    }
                }
                //Inserting value to the roomDatabase using nodeViewModel
                nvm.insert(item);
                nvm.insert(new ShoppingList(id,name));
                //Take you to the shopping list activity
                startActivity(intent);
            }
        });

    }
    private void setItemList()
    {
        itemList.add(new Item(1,"Pizza",0));
        itemList.add(new Item(2,"Oil",0));
        itemList.add(new Item(3,"kali mirch",0));
        itemList.add(new Item(4,"Rice",0));
        itemList.add(new Item(5,"Salt",0));
        itemList.add(new Item(6,"Pizza",0));
        itemList.add(new Item(7,"Oil",0));
        itemList.add(new Item(8,"kali mirch",0));
        itemList.add(new Item(9,"Rice",0));
        itemList.add(new Item(10,"Salt",0));
        itemList.add(new Item(11,"Pizza",0));
        itemList.add(new Item(12,"Oil",0));
        itemList.add(new Item(13,"kali mirch",0));
        itemList.add(new Item(14,"Rice",0));
        itemList.add(new Item(15,"Salt",0));
        itemList.add(new Item(16,"Pizza",0));
        itemList.add(new Item(17,"Oil",0));
        itemList.add(new Item(18,"kali mirch",0));
        itemList.add(new Item(19,"Rice",0));
        itemList.add(new Item(20,"Salt",0));
    }
}