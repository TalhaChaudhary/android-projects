package com.example.myshoppinglist;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.List;


public class TestActivity extends AppCompatActivity {
    private String[] placeName;
    private String[] imageUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        //new GetPlaces(this).execute();
        //Places.initialize(getApplicationContext(), "AIzaSyCGCMGM-yAFvyfmjS_u4HWn2ZuUQwdP-PQ");
        // Create a new PlacesClient instance
        //PlacesClient placesClient = Places.createClient(this);
        //new GetPlaces(this).execute();

    }



    class GetPlaces extends AsyncTask<Void, Void, Void> {
        Context context;
        //private ListView listView;
        private ProgressDialog bar;
        public GetPlaces(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
           // this.listView = listView;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
           // bar.dismiss();
            //this.listView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, placeName));
            Toast.makeText(getApplicationContext(),"placeName length : " + placeName.length,Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            //bar =  new ProgressDialog(context);
            //bar.setIndeterminate(true);
            //bar.setTitle("Loading");
            //bar.show();


        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            findNearLocation();
            //Toast.makeText(getApplicationContext(),"placeName length : "+ placeName.length,Toast.LENGTH_LONG).show();
            return null;
        }

    }
    public void findNearLocation()   {

        PlacesService service = new PlacesService("AIzaSyAnC8K4POyTUe1BoHfSk85s9wDIstJqttE");

       /*
        Hear you should call the method find nearst place near to central park new delhi then we pass the lat and lang of central park. hear you can be pass you current location lat and lang.The third argument is used to set the specific place if you pass the atm the it will return the list of nearest atm list. if you want to get the every thing then you should be pass "" only
       */


        List<Place> findPlaces = service.findPlaces(31.582045,72.329376,"atm");
        // Hear third argument, we pass the atm for getting atm , if you pass the hospital then this method return list of hospital , If you pass nothing then it will return all landmark

        placeName = new String[findPlaces.size()];
        imageUrl = new String[findPlaces.size()];

        for (int i = 0; i < findPlaces.size(); i++) {

            Place placeDetail = findPlaces.get(i);
            placeDetail.getIcon();

            System.out.println(  placeDetail.getName());
            placeName[i] =placeDetail.getName();

            imageUrl[i] =placeDetail.getIcon();
        }
    }

}